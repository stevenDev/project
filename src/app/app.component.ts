import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loginForm: FormGroup;
  listData: Array<any> = [];
  bandera: boolean = true;
  title = 'project';
  constructor(private db: AngularFireDatabase, private formBuilder: FormBuilder) {
  }
  ngOnInit() {
    this.db.list('prueba/').valueChanges().subscribe(value => {
      this.listData = value;
      console.log(this.listData);
    })
    this.loginForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      city: ['', Validators.required],
      cc: ['', Validators.required],
      email: ['', Validators.required]
    });
  }
  register() {
    const f = this.loginForm.value;
    this.db.database.ref('prueba/').push(f);
  }
  change() {
    if (this.bandera) {
      this.bandera = false;
    } else {
      this.bandera = true;
    }
  }
}
